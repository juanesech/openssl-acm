FROM alpine

RUN apk add --update openssl
ADD openssl.cnf /etc/ssl/openssl.cnf

ENTRYPOINT ["openssl"]
